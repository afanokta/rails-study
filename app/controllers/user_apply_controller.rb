class UserApplyController < ApplicationController
  before_action :set_user_apply, only: [:show, :update, :destroy]

  def index
    @companies = UserApply.all
    render json: @companies.map { |user_apply| user_apply.new_attributes }
  end

  def create
    @user_apply = UserApply.new(user_apply_params)
    if !@user_apply.save
      return render json: @user_apply.errors, status: :unprocessable_entity
    end
    render json: @user_apply.new_attributes, status: :ok
  end

  def show
    render json: @user_apply.new_attributes
  end

  def update
    if !@user_apply.update(user_apply_params)
      return render json: @user_apply.errors, status: :unprocessable_entity
    end
    render json: @user_apply.new_attributes, status: :ok
  end

  def destroy
    if !@user_apply.destroy
      return render json: @user_apply.errors, status: :unprocessable_entity
    end
    render json: @user_apply.new_attributes, status: :ok
  end

  private
  def set_user_apply
    @user_apply = UserApply.find_by_id(params[:id])
    if @user_apply.nil?
      render json: { error: "user_apply not found" }, status: :not_found
    end
    # return render json: @user_apply
  end

  def user_apply_params
    params.require(:user_apply).permit(:user_id, :job_id, :step_id)
  end
end
