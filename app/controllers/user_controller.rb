class UserController < ApplicationController
  skip_before_action :authenticate_request, only: [:login, :create]
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    @users = User.all
    render json: @users.map { |user| user.new_attributes }
  end

  def create
    @user = User.new(user_params)
    # return render json: @user
    if !@user.save
      return render json: @user.errors, status: :unprocessable_entity
    end
    render json: @user.new_attributes, status: :created
  end

  def show
    render json: @user.new_attributes
  end

  def update
    if !@user.update(user_params)
      return render json: @user.errors, status: :unprocessable_entity
    end
    render json: @user.new_attributes, status: :ok
  end

  def destroy
    if !@user.destroy
      return render json: @user.errors, status: :unprocessable_entity
    end
    render json: @user.new_attributes, status: :ok
  end

  def login
    @user = User.find_by(name: params[:username])
    # return render json: @user
    if @user && @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      render json: {
        user: @user.new_attributes,
        token: token,
      }
    else
      render json: { error: "Invalid username or password" }, status: :unauthorized
    end
  end

  private
  def set_user
    @user = User.find_by_id(params[:id])
    if @user.nil?
      render json: {error: "User not found"}, status: :not_found
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :nik, :phone_number, :address, :dob, :password, :company_id, :role)
  end
end
