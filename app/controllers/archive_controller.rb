class ArchiveController < ApplicationController
  before_action :set_archive, only: [:show, :update, :destroy]

  def index
    @archives = Archive.all
    render json: @archives.map { |archive| archive.new_attributes }
  end

  def create
    @archive = Archive.new(archive_params)
    if !@archive.save
      return render json: @archive.errors, status: :unprocessable_entity
    end
    render json: @archive.new_attributes, status: :created
  end

  def show
    @archive = Archive.find(params[:id])
    render json: @archive.new_attributes, status: :ok
  end

  def update
    if !@archive.update(archive_params)
      return render json: @archive.erros, status: :unprocessable_entity
    end
    render json: @archive.new_attributes, status: :ok
  end

  def destroy
    if !@archive.destroy
      return render json: @archive.errors, status: :unprocessable_entity
    end
    render json: @archive.new_attributes, status: :ok
  end

  private
  def set_archive
    @archive = Archive.find_by_id(params[:id])
    if @archive.nil?
      render json: { error: "archive not found" }, status: :not_found
    end
  end

  def archive_params
    params.require(:archive).permit(:name, :store_at, :user_id)
  end
end
