class StepController < ApplicationController
  before_action :set_step, only: [:show, :update, :destroy]

  def index
    @steps = Step.all
    render json: @steps.map { |step| step.new_attributes }
  end

  def create
    @step = Step.new(step_params)
    if !@step.save
      return render json: @step.errors, status: :unprocessable_entity
    end
    render json: @step.new_attributes, status: :created
  end

  def show
    render json: @step.new_attributes
  end

  def update
    if !@step.update(step_params)
      return render json: @step.errors, status: :unprocessable_entity
    end
    render json: @step.new_attributes, status: :created
  end

  def destroy
    if !@step.destroy
      return render json: @step.errors, status: :unprocessable_entity
    end
    render json: @step.new_attributes, status: :created
  end

  private
  def set_step
    @step = Step.find_by_id(params[:id])
    if @step.nil?
      render json: { status: "error, step not found" }
    end
  end

  def step_params
    params.require(:step).permit(:name)
  end
end
