class JobController < ApplicationController
  before_action :set_job, only: [:show, :update, :destroy]

  def index
    @jobs = Job.all
    render json: @jobs.map { |job| job.new_attributes }
  end

  def create
    @job = Job.new(job_params)
    if !@job.save
      return render json: @job.errors, status: :unprocessable_entity
    end
    render json: @job.new_attributes, status: :created
  end

  def show
    render json: @job.new_attributes
  end

  def update
    if !@job.update(job_params)
      return render json: @job.errors, status: :unprocessable_entity
    end
    render json: @job.new_attributes, status: :ok
  end

  def destroy
    if !@job.destroy
      return render json: @job.errors, status: :unprocessable_entity
    end
    render json: @job.new_attributes, status: :ok
  end

  private
  def set_job
    @job = Job.find_by_id(params[:id])
    if @job.nil?
      render json: {status: "error, job not found"}
    end
  end

  def job_params
    params.require(:job).permit(:position, :company_id, :description, :start_date, :end_date)
  end
end
