class CompanyController < ApplicationController
  before_action :set_company, only: [:show, :update, :destroy]

  def index
    @companies = Company.all
    render json: @companies.map { |company| company.new_attributes }
  end

  def create
    @company = Company.new(company_params)
    if !@company.save
      return render json: @company.errors, status: :unprocessable_entity
    end
    render json: @company.new_attributes, status: :ok
  end

  def show
    render json: @company.new_attributes
  end

  def update
    if !@company.update(company_params)
      return render json: @company.errors, status: :unprocessable_entity
    end
    render json: @company.new_attributes, status: :ok
  end

  def destroy
    if !@company.destroy
      return render json: @company.errors, status: :unprocessable_entity
    end
    render json: @company.new_attributes, status: :ok
  end

  private
  def set_company
    @company = Company.find_by_id(params[:id])
    if @company.nil?
      render json: { error: "company not found" }, status: :not_found
    end
    # return render json: @company
  end

  def company_params
    params.require(:company).permit(:name, :address)
  end
end
