class User < ApplicationRecord
  has_secure_password

  belongs_to :company
  has_many :archives, dependent: :destroy

  has_many :user_applies
  has_many :jobs, through: :user_applies, source: :job

  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i },
                    uniqueness: true
  validates :nik, presence: true, length: {  maximum: 16 },
                            format: { with: /\A[0-9]+\z/ },
                            uniqueness: true
  # validates :phone_number, presence: true, length: {  maximum: 15 },
  #                           format: { with: /\A[0-9]+\z/ },
  #                           uniqueness: true
  validates :dob, presence: true
  validates :address, presence: true

  enum role: {
    user: 0,
    recruiter: 1,
    admin: 2
  }

  # self.ignored_columns = [:password_digest]

  def new_attributes
    {
      id: self.id,
      name: self.name,
      email: self.email,
      nik: self.nik,
      phone_number: self.phone_number,
      address: self.address,
      dob: self.dob,
      company: self.company,
      role: self.role,
      created_at: self.created_at
    }
  end
end
