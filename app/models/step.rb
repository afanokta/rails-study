class Step < ApplicationRecord
  has_many :user_applies

  validates :name, presence: true, length: { maximum:20 }

  def new_attributes
    {
      id: self.id,
      name: self.name
    }
  end
end
