class Archive < ApplicationRecord
  belongs_to :user

  validates :name, presence: true, length: { maximum: 100 }
  validates :store_at, presence: true, length: { maximum: 255 }

  def new_attributes
    {
      id: self.id,
      name: self.name,
      store_at: self.store_at,
      user: self.user.new_attributes,
      created_at: self.created_at
    }
  end
end
