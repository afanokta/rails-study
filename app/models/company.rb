class Company < ApplicationRecord
  has_many :users, dependent: :destroy

  has_many :jobs, dependent: :destroy

  validates :name, presence: true, length: { maximum: 100 }
  validates :address, presence: true, length: { maximum: 100 }

  def new_attributes
    {
      id: self.id,
      name: self.name,
      address: self.address,
      created_at: self.created_at,
    }
  end

end
