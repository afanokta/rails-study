class Job < ApplicationRecord

  has_many :user_applies
  has_many :users, through: :user_applies, source: :user

  belongs_to :company

  validates :position, presence: true, length: { maximum: 30 }
  validates :description, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  def new_attributes
    {
      id: self.id,
      position: self.position,
      description: self.description,
      start_date: self.start_date,
      end_date: self.end_date,
      company: self.company
      # users: self.users
    }
  end
end
