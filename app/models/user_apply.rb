class UserApply < ApplicationRecord
  belongs_to :step
  belongs_to :job
  belongs_to :user

  validates :user_id, presence: true
  validates :job_id, presence: true
  validates :step_id, presence: true

  def new_attributes
    {
      id: self.id,
      user: self.user,
      job: self.job,
      step: self.step
    }
  end
end
