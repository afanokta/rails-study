Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  post '/login', to: "user#login"
  resources :job
  resources :user
  resources :archive
  resources :step
  resources :company
  resources :user_apply
end
