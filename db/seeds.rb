# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# Company.create([
#   {
#     "name": 'Qatros Teknologi Nusantara',
#     "address": 'Sleman'
#   },
#   {
#     "name": 'Arkatama',
#     "address": 'Malang'
#   }
# ])

# User.create([
#   {
#     "name": 'booby',
#     "email": 'bobby@gmail.com',
#     "nik": '350405091001002',
#     "phone_number": '089632881882',
#     "address": 'Tulungagung',
#     "dob": '2001-10-10',
#     "password_digest": 'bobby1234',
#     "company_id": 1
#   },
#   {
#     "name": 'afan',
#     "email": 'afan@gmail.com',
#     "nik": '350405091001001',
#     "phone_number": '089632881881',
#     "address": 'Tulungagung',
#     "dob": '2001-10-10',
#     "password_digest": 'afan1234',
#     "company_id": 1
#   }
# ])

Step.create([
  {
    "name": 'Screening'
  },
  {
    "name": 'Interview'
  },
  {
    "name": 'Offer'
  },
  {
    "name": 'Accept'
  },
  {
    "name": 'Reject'
  }
])

# Job.create([
#   {
#     "position": 'Backend Developer',
#     "description": 'Lorem Ipsum ....',
#     "start_date": '2023-02-16',
#     "company_id": 1,
#     "end_date": '2023-06-30'
#   },
#   {
#     "position": 'FrontEnd Developer',
#     "description": 'Lorem Ipsum ....',
#     "start_date": '2023-02-16',
#     "company_id": 1,
#     "end_date": '2023-06-30'
#   }
# ])

# Archive.create([
#   {
#     "name": 'CV Booby',
#     "store_at": '/files/CV_Booby',
#     "user_id": 1
#   },
#   {
#     "name": 'NPWP Booby',
#     "store_at": '/files/NPWP_Booby',
#     "user_id": 1
#   },
#   {
#     "name": 'CV Afan',
#     "store_at": '/files/CV_Afan',
#     "user_id": 2
#   }
# ])

# UserApply.create([
#   {
#     "job_id": 1,
#     "user_id": 1,
#     "step_id": 1
#   }
# ])

5.times do
  Company.create(
    "name": Faker::Company.name,
    "address": Faker::Address.city,
  )
end

# company = Company.all

10.times do
  User.create(
    "name": Faker::Name.name,
    "email": Faker::Internet.email,
    "phone_number": Faker::PhoneNumber.phone_number,
    "address": Faker::Address.city,
    "nik": Faker::Number.number(digits: 16),
    "dob": Faker::Date.birthday(min_age: 18, max_age: 50),
    "password": "password",
    "company_id": Company.all.sample.id,
    "role": Faker::Number.between(from: 0, to: 1)
  )
end

5.times do
  Job.create(
    "position": Faker::Job.position,
    "company_id": Company.all.sample.id,
    "description": Faker::Lorem.paragraph,
    "start_date": Faker::Date.between(from: '2022-08-01', to: '2023-07-01'),
    "end_date": Faker::Date.between(from: '2023-08-01', to: '2024-07-01')
  )
end

10.times do
  Archive.create(
    "name": Faker::File.file_name(dir: ''),
    "store_at": Faker::File.file_name(dir: 'path/to'),
    "user_id": User.all.sample.id
  )
end

5.times do
  UserApply.create(
    "user_id": User.all.sample.id,
    "job_id": Job.all.sample.id,
    "step_id": Step.all.sample.id
  )
end

p "Berhasil menjalankan seed"
