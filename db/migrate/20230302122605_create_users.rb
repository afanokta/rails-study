class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :nik
      t.string :phone_number
      t.string :address
      t.date   :dob
      t.string :password_digest
      t.integer :company_id
      t.integer :role, default: 0

      t.timestamps
    end
  end
end
