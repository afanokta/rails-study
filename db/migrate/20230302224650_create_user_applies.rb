class CreateUserApplies < ActiveRecord::Migration[7.0]
  def change
    create_table :user_applies do |t|
      t.integer :user_id
      t.integer :job_id
      t.integer :step_id

      t.timestamps
    end
  end
end
